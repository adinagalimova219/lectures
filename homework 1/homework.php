<?php
// 1. Дано число. Проверьте, отрицательное оно или нет. Выведите об этом информацию в консоль.
function neg($a) {
    if ($a >= 0) {
        echo "Число положительное\n";
    } else {
        echo "Число отрицательное\n";
    }
} 
neg(-2);

// 2. Дана строка. Выведите в консоль длину этой строки.
function len($str) {
    echo strlen($str) . "\n";
}
len("hello world!");

// 3. Дана строка. Проверьте, есть ли в этой строке символ 'a'. Если есть, то выведите 'да', если нет, то 'нет'.
function findletter($str) {
    $found = false;
    for ($i = 0; $i < strlen($str); $i++) {
        if ($str[$i] == "a") {
            $found = true;
            break;
}
}
if ($found) {
    echo "да\n";
} else {
    echo "нет\n";
}
}
findletter("apple");

// 4. Дано число. Проверьте, что оно делится на 2, на 3, на 5, на 6, на 9 без остатка.
function mod1($nb) {
    if ($nb%2 == 0 && $nb%3 == 0 && $nb%5 == 0 && $nb%6 == 0 && $nb%9 == 0) {
        echo "без остатка\n";
    } else {
        echo "с остатком\n";
    }
}
mod1(180);

// 5. Дано число. Проверьте, что оно делится на 3, на 5, на 7, на 11 без остатка.
function mod2($nb) {
    if ($nb%3 == 0 && $nb%5 == 0 && $nb%7 == 0 && $nb%11 == 0) {
        echo "без остатка\n";
    } else {
        echo "с остатком\n";
    }
}
mod2(180);

// 6. Дана строка. Выведите в консоль последний символ строки.
function lastsymbol($str) {
    echo $str[strlen($str)-1] . "\n";
}
lastsymbol("hello world!");

// 8. Дан треугольник. Выведите в консоль его площадь.
function area($a, $h) {
    $area = 0.5 * $h * $a; 
    echo $area . "\n";
}
area(4,5);

// 9. Дан прямоугольник. Выведите в консоль его площадь.
function area2($a, $b) {
    $area = $a * $b;
    echo $area . "\n";
}
area2(4,5);

// 10. Дано число. Выведите в консоль квадрат этого числа.
function square($nb) {
    $sq = $nb * $nb;
    echo $sq . "\n";
}
square(5);
?>