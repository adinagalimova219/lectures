<?php

namespace App\Http\Controllers;

use App\Models\Task;

class TaskController extends Controller {

    public function lecture2() {

        $tasks = Task::all();
        
        return view('lecture2', compact('tasks'));

    }

}


