<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Task extends Model {
    use HasFactory;
    public $id;
    public $name;
    public $status;
    public $priority;
    public $deadline;
    protected $table = 'tasks';
    protected $fillable = ['name', 'status', 'priority', 'deadline'];
}
?>
