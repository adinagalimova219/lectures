<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Task extends Model {
    public $id;
    public $name;
    public $status;
    public $priority;
    public $deadline;
    protected $table = 'tasks';
}
?>